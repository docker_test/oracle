## docker compose for mssql

```
docker-compose up
```

# Dockerfile
FROM oracleinanutshell/oracle-xe-11g

ADD init.sql /docker-entrypoint-initdb.d/
ADD script.sh /docker-entrypoint-initdb.d/

login
```
hostname: localhost
port: 49161
sid: xe
username: system
password: oracle
```

# Login http://localhost:8080/apex/apex_admin with following credential:

username: ADMIN
password: Oracle_11g